import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { MainComponent } from './components/main/main.component';
import { HttpClientModule } from "@angular/common/http";
import {MatSelectModule} from "@angular/material/select";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NgSelectModule} from '@ng-select/ng-select';
import {ExpressPointService} from "./services/express-point.service";
import { UserFormComponent } from './components/forms/user-form/user-form.component';
import { RegneFormComponent } from './components/forms/regne-form/regne-form.component';
import { EmbranchementFormComponent } from './components/forms/embranchement-form/embranchement-form.component';
import { ClasseFormComponent } from './components/forms/classe-form/classe-form.component';
import { OrdreFormComponent } from './components/forms/ordre-form/ordre-form.component';
import { FamilleFormComponent } from './components/forms/famille-form/famille-form.component';
import { GenreFormComponent } from './components/forms/genre-form/genre-form.component';
import { EspeceFormComponent } from './components/forms/espece-form/espece-form.component';
import { SentinelleFormComponent } from './components/forms/sentinelle-form/sentinelle-form.component';
import { AlerteFormComponent } from './components/forms/alerte-form/alerte-form.component';
import { ProjetsauvetageFormComponent } from './components/forms/projetsauvetage-form/projetsauvetage-form.component';
import { NarrateurFormComponent } from './components/forms/narrateur-form/narrateur-form.component';
import { LivresauvetageFormComponent } from './components/forms/livresauvetage-form/livresauvetage-form.component';
import { TacheFormComponent } from './components/forms/tache-form/tache-form.component';
import { MouvementlotFormComponent } from './components/forms/mouvementlot-form/mouvementlot-form.component';
import { LotFormComponent } from './components/forms/lot-form/lot-form.component';
import { SallestockageFormComponent } from './components/forms/sallestockage-form/sallestockage-form.component';
import { PaysFormComponent } from './components/forms/pays-form/pays-form.component';
import { CommuneFormComponent } from './components/forms/commune-form/commune-form.component';
import { SiteFormComponent } from './components/forms/site-form/site-form.component';
import { SalarieFormComponent } from './components/forms/salarie-form/salarie-form.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    UserFormComponent,
    RegneFormComponent,
    EmbranchementFormComponent,
    ClasseFormComponent,
    OrdreFormComponent,
    FamilleFormComponent,
    GenreFormComponent,
    EspeceFormComponent,
    SentinelleFormComponent,
    AlerteFormComponent,
    ProjetsauvetageFormComponent,
    NarrateurFormComponent,
    LivresauvetageFormComponent,
    TacheFormComponent,
    MouvementlotFormComponent,
    LotFormComponent,
    SallestockageFormComponent,
    PaysFormComponent,
    CommuneFormComponent,
    SiteFormComponent,
    SalarieFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    BrowserAnimationsModule,
    NgSelectModule
  ],
  providers: [
    ExpressPointService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
