import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {ExpressPointService} from "../../services/express-point.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private expressService: ExpressPointService
  ) {  }

  public result: User = {};
  public requestBody: User = {};
  public requestResult: Array<any> = [{}];

  public tableArray$: Array<any> = [{
      "url": "none",
      "tableName": "pasDeTable"
    }];
  public tableToUse: any = "Select a table"

  ngOnInit(): void {
    this.setTables();
  }

  public changeTableInUse() {
    console.log('Changement de table', this.tableToUse);
    const url: string = this.tableArray$.find(table => table.tableName === this.tableToUse).url ?? "error";
    console.log('Nouvelle URL : ',url);
    this.expressService.changeURL(url);
    console.log('Using API url : ', this.expressService.urlAPI);
    this.showTable()
  }

  public setTables() {
    this.expressService.getRoutes()
      .subscribe(response => {
        this.tableArray$ = response ;
        }, err => {
        console.log('error : ', err);
        });
  }

  public showTable() {
    this.expressService.showUsers()
      .subscribe((result) => {
        this.requestResult = result
      })
  }

}

interface User {
  id?: number,
  firstName?: string,
  lastName?: string,
  age?: number
}
