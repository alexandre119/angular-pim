import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SallestockageFormComponent } from './sallestockage-form.component';

describe('SallestockageFormComponent', () => {
  let component: SallestockageFormComponent;
  let fixture: ComponentFixture<SallestockageFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SallestockageFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SallestockageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
