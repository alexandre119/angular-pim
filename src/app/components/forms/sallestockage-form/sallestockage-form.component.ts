import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Espece, SalleStockage} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-sallestockage-form',
  templateUrl: './sallestockage-form.component.html',
  styleUrls: ['./sallestockage-form.component.css']
})
export class SallestockageFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: SalleStockage = {  };

  public requestBody: SalleStockage = {  };

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    name: ["", Validators.required],
    luminosite:["",[Validators.pattern("[0-9]+"),Validators.min(0)]],
    temperature:["",[Validators.pattern("[0-9]+")]],
    humidite:["",[Validators.pattern("[0-9]+"),Validators.min(0),Validators.max(100)]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      name: this.result.name,
      luminosite: this.result.luminosite,
      humidite: this.result.humidite,
      temperature: this.result.temperature
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
