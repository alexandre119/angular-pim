import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {UniversalModel} from "../../../services/form-interfaces.service"
import {FormSendingService} from "../../../services/form-sending.service";

@Component({
  selector: 'app-regne-form',
  templateUrl: './regne-form.component.html',
  styleUrls: ['./regne-form.component.css']
})
export class RegneFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: UniversalModel = {
    id: undefined,
    name: undefined
  };

  public requestBody: UniversalModel = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    name: ["", Validators.required],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      name: this.result.name,
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
