import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegneFormComponent } from './regne-form.component';

describe('RegneFormComponent', () => {
  let component: RegneFormComponent;
  let fixture: ComponentFixture<RegneFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegneFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegneFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
