import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SentinelleFormComponent } from './sentinelle-form.component';

describe('SentinelleFormComponent', () => {
  let component: SentinelleFormComponent;
  let fixture: ComponentFixture<SentinelleFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SentinelleFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SentinelleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
