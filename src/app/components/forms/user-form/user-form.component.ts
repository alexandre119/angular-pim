import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {User} from "../../../services/form-interfaces.service"
import {FormSendingService} from "../../../services/form-sending.service";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: User = {
    id: undefined,
    firstName: undefined,
    lastName: undefined,
    age: undefined
  };

  public requestBody: User = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    firstName: ["", Validators.required],
    lastName: ["", Validators.required],
    age: ["",[Validators.pattern("[0-9]+"),Validators.min(0), Validators.required]]
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      firstName: this.result.firstName,
      lastName: this.result.lastName,
      age: this.result.age
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
