import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlerteFormComponent } from './alerte-form.component';

describe('AlerteFormComponent', () => {
  let component: AlerteFormComponent;
  let fixture: ComponentFixture<AlerteFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlerteFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlerteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
