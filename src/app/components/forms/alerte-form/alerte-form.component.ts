import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Alerte} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-alerte-form',
  templateUrl: './alerte-form.component.html',
  styleUrls: ['./alerte-form.component.css']
})
export class AlerteFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: Alerte = {
    id: undefined,
    name: undefined,
    description: undefined,
    sentinelle: undefined,
    espece: undefined
  };

  public requestBody: Alerte = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    name: ["", Validators.required],
    description: ["", Validators.required],
    sentinelle:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
    espece:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      name: this.result.name,
      description: this.result.description,
      sentinelle: this.result.sentinelle,
      espece: this.result.espece
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
