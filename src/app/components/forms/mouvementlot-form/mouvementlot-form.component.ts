import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Espece, MouvementLot} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-mouvementlot-form',
  templateUrl: './mouvementlot-form.component.html',
  styleUrls: ['./mouvementlot-form.component.css']
})
export class MouvementlotFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: MouvementLot = {
    id: undefined
  };

  public requestBody: MouvementLot = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    date: ["", Validators.required],
    destination: ["", Validators.required],
    lot:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
    salarie:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
    tache:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      lot: this.result.lot,
      salarie: this.result.salarie,
      tache: this.result.tache,
      date: this.result.date,
      destination: this.result.destination
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
