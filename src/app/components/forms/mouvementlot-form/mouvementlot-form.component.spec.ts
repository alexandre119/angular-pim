import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MouvementlotFormComponent } from './mouvementlot-form.component';

describe('MouvementlotFormComponent', () => {
  let component: MouvementlotFormComponent;
  let fixture: ComponentFixture<MouvementlotFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MouvementlotFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MouvementlotFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
