import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ExpressPointService} from "../../../services/express-point.service";
import {Embranchement, UniversalModel} from "../../../services/form-interfaces.service";
import {FormSendingService} from "../../../services/form-sending.service";

@Component({
  selector: 'app-embranchement-form',
  templateUrl: './embranchement-form.component.html',
  styleUrls: ['./embranchement-form.component.css']
})
export class EmbranchementFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: Embranchement = {
    id: undefined,
    name: undefined,
    regne: undefined
  };

  public requestBody: Embranchement = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    name: ["", Validators.required],
    regne:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      name: this.result.name,
      regne: this.result.regne
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
