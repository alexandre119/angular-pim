import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbranchementFormComponent } from './embranchement-form.component';

describe('EmbranchementFormComponent', () => {
  let component: EmbranchementFormComponent;
  let fixture: ComponentFixture<EmbranchementFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmbranchementFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbranchementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
