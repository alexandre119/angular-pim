import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Espece, Lot} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-lot-form',
  templateUrl: './lot-form.component.html',
  styleUrls: ['./lot-form.component.css']
})
export class LotFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: Lot = {  };

  public requestBody: Lot = {  };

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    espece:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
    salle:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      salle: this.result.salle,
      espece: this.result.espece
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
