import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Espece, LivreSauvetage} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-livresauvetage-form',
  templateUrl: './livresauvetage-form.component.html',
  styleUrls: ['./livresauvetage-form.component.css']
})
export class LivresauvetageFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: LivreSauvetage = {  };

  public requestBody: LivreSauvetage = {  };

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    name: ["", Validators.required],
    info: ["", Validators.required],
    projetSauvetage:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      name: this.result.name,
      info: this.result.info,
      projetSauvetage: this.result.projetSauvetage
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
