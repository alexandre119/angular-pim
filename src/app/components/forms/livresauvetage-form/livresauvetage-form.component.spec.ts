import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LivresauvetageFormComponent } from './livresauvetage-form.component';

describe('LivresauvetageFormComponent', () => {
  let component: LivresauvetageFormComponent;
  let fixture: ComponentFixture<LivresauvetageFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LivresauvetageFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LivresauvetageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
