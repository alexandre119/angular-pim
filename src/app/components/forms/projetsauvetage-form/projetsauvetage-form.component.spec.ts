import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetsauvetageFormComponent } from './projetsauvetage-form.component';

describe('ProjetsauvetageFormComponent', () => {
  let component: ProjetsauvetageFormComponent;
  let fixture: ComponentFixture<ProjetsauvetageFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjetsauvetageFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetsauvetageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
