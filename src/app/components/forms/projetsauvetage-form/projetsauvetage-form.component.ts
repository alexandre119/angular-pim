import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Espece, ProjetSauvetage} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-projetsauvetage-form',
  templateUrl: './projetsauvetage-form.component.html',
  styleUrls: ['./projetsauvetage-form.component.css']
})
export class ProjetsauvetageFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: ProjetSauvetage = {
    id: undefined,
    name: undefined,
    description: undefined,
    alerte: undefined,
    narrateur: undefined
  };

  public requestBody: ProjetSauvetage = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    name: ["", Validators.required],
    description: ["", Validators.required],
    alerte:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
    narrateur:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      name: this.result.name,
      description: this.result.description,
      alerte: this.result.alerte,
      narrateur: this.result.narrateur
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
