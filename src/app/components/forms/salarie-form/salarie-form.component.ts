import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Espece, Salarie} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-salarie-form',
  templateUrl: './salarie-form.component.html',
  styleUrls: ['./salarie-form.component.css']
})
export class SalarieFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: Salarie = {
  };

  public requestBody: Salarie = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    prenom: ["", Validators.required],
    name: ["", Validators.required],
    adresse: ["", Validators.required],
    site:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      prenom: this.result.prenom,
      name: this.result.name,
      adresse: this.result.adresse,
      site: this.result.site
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
