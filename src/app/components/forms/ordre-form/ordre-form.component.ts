import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Ordre, UniversalModel} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-ordre-form',
  templateUrl: './ordre-form.component.html',
  styleUrls: ['./ordre-form.component.css']
})
export class OrdreFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: Ordre = {
    id: undefined,
    name: undefined,
    classe: undefined
  };

  public requestBody: Ordre = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    name: ["", Validators.required],
    classe:["",[Validators.pattern("[0-9]+"),Validators.min(1),Validators.required]],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      name: this.result.name,
      classe: this.result.classe
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
