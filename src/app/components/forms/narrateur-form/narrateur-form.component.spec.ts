import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NarrateurFormComponent } from './narrateur-form.component';

describe('NarrateurFormComponent', () => {
  let component: NarrateurFormComponent;
  let fixture: ComponentFixture<NarrateurFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NarrateurFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NarrateurFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
