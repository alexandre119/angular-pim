import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormSendingService} from "../../../services/form-sending.service";
import {Espece, Narrateur} from "../../../services/form-interfaces.service";

@Component({
  selector: 'app-narrateur-form',
  templateUrl: './narrateur-form.component.html',
  styleUrls: ['./narrateur-form.component.css']
})
export class NarrateurFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private fs: FormSendingService,
  ) {  };

  ngOnInit(): void {  };

  public result: Narrateur = {
    id: undefined,
    name: undefined
  };

  public requestBody: Narrateur = {};

  public customForm = this.fb.group({
    id:["",[Validators.pattern("[0-9]+"),Validators.min(1)]],
    name: ["", Validators.required],
  });

  public submitForm() {
    this.result = this.customForm.getRawValue();
    this.requestBody = {
      name: this.result.name
    };
  };

  public confirmSubmit() {
    this.fs.submit(this.requestBody);
  };

  public confirmUpdate() {
    if ( this.result.id !== undefined) {
      this.fs.update(this.requestBody, this.result.id)
    }
    else {
      console.log('Update impossible, id undefined')
    }
  };
}
