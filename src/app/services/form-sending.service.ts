import { Injectable } from '@angular/core';
import {ExpressPointService} from "./express-point.service";
import {MainComponent} from "../components/main/main.component";

@Injectable({
  providedIn: 'root'
})
export class FormSendingService {

  constructor(
    private expressService: ExpressPointService
  ) { }

  public submit(body: any) {
    console.log('Request : ' , body)
    this.expressService.addUser(body)
      .subscribe(response => {
        console.log('Response : ', response);
      }, err => {
        console.log('error : ', err);
      }, () => {
        console.log('Add complete');
      });
  };

  public update(body: any, id: number | null) {
    if (id === null) {
      console.log('ERROR: ID null')
    }
    else {
      console.log('Request : ', body)
      this.expressService.updateUser(body, id)
        .subscribe(response => {
          console.log('response : ', response);
        }, err => {
          console.log('error : ', err);
        }, () => {
          console.log('Update complete');
        });
    }
  };
}
