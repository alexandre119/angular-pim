import { TestBed } from '@angular/core/testing';

import { FormSendingService } from './form-sending.service';

describe('FormSendingService', () => {
  let service: FormSendingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormSendingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
