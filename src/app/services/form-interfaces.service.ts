import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormInterfacesService {

  constructor() { }
}

export interface UniversalModel {
  id?: number,
  name?: string
}

export interface Embranchement extends UniversalModel {
  regne?: number
}

export interface Classe extends UniversalModel {
  embranchement?: number
}

export interface Ordre extends UniversalModel {
  classe?: number
}

export interface Famille extends UniversalModel {
  ordre?: number
}

export interface Genre extends UniversalModel {
  famille?: number
}

export interface Espece extends UniversalModel {
  genre?: number
}

export interface Narrateur extends UniversalModel {  }

export interface Sentinelle extends UniversalModel {  }

export interface Alerte extends UniversalModel {
  description?: string,
  sentinelle?: number,
  espece?: number
}

export interface ProjetSauvetage extends UniversalModel {
  description?: string,
  alerte?: number,
  narrateur?: number
}

export interface LivreSauvetage extends UniversalModel {
  info?: string,
  projetSauvetage?: number
}

export interface Tache extends UniversalModel {
  type?: string,
  description?: string,
  projetSauvetage?: number
}

export interface SalleStockage extends UniversalModel {
  luminosite?: number
  humidite?: number,
  temperature?: number
}

export interface Lot {
  id?: number,
  espece?: number,
  salle?: number
}

export interface MouvementLot {
  id?: number,
  lot?: number,
  tache?: number,
  salarie?: number,
  date?: string,
  destination?: string
}

export interface Pays extends UniversalModel {
  code?: string
}

export interface Commune extends UniversalModel {
  pays?: number
}

export interface Site extends UniversalModel {
  commune?: number
}

export interface Salarie {
  id?: number,
  name?: string,
  prenom?: string,
  adresse?: string,
  site?: number
}

export interface User {
  id?: number,
  firstName?: string,
  lastName?: string,
  age?: number
}
