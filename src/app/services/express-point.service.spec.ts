import { TestBed } from '@angular/core/testing';

import { ExpressPointService } from './express-point.service';

describe('ExpressPointService', () => {
  let service: ExpressPointService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExpressPointService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
