import { TestBed } from '@angular/core/testing';

import { FormInterfacesService } from './form-interfaces.service';

describe('FormInterfacesService', () => {
  let service: FormInterfacesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormInterfacesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
