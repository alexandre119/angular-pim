import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from 'rxjs';
import {catchError, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ExpressPointService {
  baseURL: string = 'http://localhost:9999/'
  urlAPI: string = 'http://localhost:9999/users'
  public totalAngularPackages: any;
  public result: any;

  constructor(
    public http: HttpClient
  ) { }

  public changeURL(url: string) {
    this.urlAPI = this.baseURL + url
  }

  private static handleError(error: HttpErrorResponse) {
    console.log(error)
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-form-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  public addUser(body: any): Observable<any> {
    console.log("Le body dans la fn POST ADD : ",JSON.stringify(body));
    return this.http.post<any>(this.urlAPI, body, {headers: {
        ContentType: "application/json"
      }})
      .pipe(
        catchError((err) => ExpressPointService.handleError(err))
      )
  }

  public updateUser(body: any, id: number): Observable<any> {
    console.log("Le body dans la fn POST UPDATE : ",JSON.stringify(body));
    const urlUpdate: string = this.urlAPI+'/'+id;
    console.log("URL update : ", urlUpdate);
    return this.http.post<any>(urlUpdate, body, {headers: {
        ContentType: "application/json"
      }})
      .pipe(
        catchError((err) => ExpressPointService.handleError(err))
      );
  }

  public showUsers(): Observable<any> {
    return this.http.get(this.urlAPI)
      .pipe(map((res: any) => res))
  }

  public showOneUser(id: number): Observable<any> {
    const urlUpdate: string = this.urlAPI+'/'+id;
    console.log("URL update : ", urlUpdate);
    return this.http.get(urlUpdate)
      .pipe(map((res: any) => res));
  }

  public getRoutes(): Observable<any> {
    return this.http.get('http://localhost:9999/getRoutes')
      .pipe(map((res: any) => res))
  }

}
